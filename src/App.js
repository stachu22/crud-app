import React, { Component } from 'react';
import IndecisionApp from './pages/IndecisonApp';


class App extends Component {
  render() {
    const title = 'Indecision';
    const subtitle = 'Put your life in the hands of a computer';
    return (
      <IndecisionApp />
    );
  };
};

export default App;
