import React from 'react';

const Action = (props) => (
  <div className="col-md-12 po">
    <button
      className="btn btn-secondary btn-lg btn-block rounded-0"
      disabled={!props.hasOptions}
      onClick={props.handlePick}
    >What should I do ?
    </button>
  </div>
);

export default Action;