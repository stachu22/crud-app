import React from 'react';

class AddOption extends React.Component {
  constructor(props) {
    super(props);

    this.handleAddOption = this.handleAddOption.bind(this);
    this.state = {
      error: undefined
    };
  }
  handleAddOption(event) {
    event.preventDefault();
    const option = event.target.elements.option.value.trim();
    const error = this.props.handleAddOption(option);

    this.setState(() => ({ error }))

    if (!error) {
      event.target.elements.option.value = '';
    }
  }

  render() {
    return (
      <div className="container">
        {this.state.error && <p>{this.state.error}</p>}
        <form className="justify-content-center row" onSubmit={this.handleAddOption}>
          <div className="col-md-5 input-pading">
            <input className="rounded-0 form-control" type="text" name="option" />
          </div>
          <div className="col-md-1">
            <button className="btn btn-secondary rounded-0" type="submit">Add option</button>
          </div>
        </form>
      </div>
    );
  }
}

export default AddOption;