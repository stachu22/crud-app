import React from 'react';

const Header = (props) => (
    <div className="justify-content-center text-center mt" >
        <h1> {props.title} </h1>
        <h2 > {props.subtitle} </h2>
    </div>
);

export default Header;