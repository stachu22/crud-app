import React from 'react';

const Option = (props) => (
  <div className="container">
    <div className="row justify-content-center">
      <div className="background-color col-md-5">
        <span>{props.optionText}</span>
      </div>
      <div className="col-md-1">
        <button
          className="btn btn-secondary margin-button rounded-0"
          onClick={(event) => {
            props.handleDeleteOption(props.optionText);
          }}
        >
          Remove</button>
      </div>
    </div>
  </div>
);

export default Option;