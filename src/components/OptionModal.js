import React from 'react';
import Modal from 'react-modal';

const OptionModal = (props) => (
    <div className="container height">
        <div className="row">
            <Modal
                className="text-center"
                isOpen={!!props.selectedOption}
                onRequestClose={props.clearSelectedOption}
                contentLabel="Selected Option"
                ariaHideApp={false}
            >
                <div className="align-self-center modal-style">
                    <h2>Selected Option</h2>
                    {props.selectedOption && <h4>{props.selectedOption}</h4>}
                    <button className="btn btn-secondary rounded-0" onClick={props.clearSelectedOption}>OK</button>
                </div>
            </Modal>
        </div>
    </div>
);

export default OptionModal;