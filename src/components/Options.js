import React from 'react';
import Option from './Option';

const Options = (props) => (
  <div className="">
    <div className="row">
    </div>
    {props.options.length === 0 && <p>Please add an option to get started !!</p>}
    <div className="margin-top-bottom2">
      <div className="container">
        <div className="justify-content-end row">
          <div className="background-color-2 col-md-5 text-center">
            <h4> Your job to do today !!!</h4>
          </div>
          <div className="col-md-4 align-self-center">
            <button className="btn btn-secondary rounded-0" onClick={props.handleDeleteOptions}>Romove All</button>
          </div>
        </div>
      </div>
      {
        props.options.map((option) => (
          <Option
            key={option}
            optionText={option}
            handleDeleteOption={props.handleDeleteOption}
          />
        ))
      }
    </div>
  </div>
);

export default Options;